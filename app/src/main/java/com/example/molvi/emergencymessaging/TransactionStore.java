package com.example.molvi.emergencymessaging;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import java.util.ArrayList;
import java.util.List;

public class TransactionStore {

    Context context;
    ContactDBHelper dbHelper ;
    SQLiteDatabase db;

    public TransactionStore(Context context){
        this.context = context;
        dbHelper = new ContactDBHelper(this.context);
        db = dbHelper.getWritableDatabase();
    }

    public long addNumber(ContentValues cv){
        return db.insert(ContactContract.ContactEntry.TABLE_CONTACT, null, cv);
    }

    List<Number> getNumbers(){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] projection = {
                BaseColumns._ID,
                ContactContract.ContactEntry.COL_NAME,
                ContactContract.ContactEntry.COL_PHONE
        };
        String selection = "*";
        Cursor cursor = db.query(ContactContract.ContactEntry.TABLE_CONTACT,
                projection,
                selection,
                null,
                null,
                null,
                null);
        List<Number> numbers = new ArrayList<>();
        while (cursor.moveToNext()){
            String name = cursor.getString(cursor.getColumnIndexOrThrow(ContactContract.ContactEntry.COL_NAME));
            String phone = cursor.getString(cursor.getColumnIndexOrThrow(ContactContract.ContactEntry.COL_PHONE));
            numbers.add(new Number(name, phone));
        }
        cursor.close();
        return numbers;
    }

    public long delete(String selection, String[] selectionArgs){
        return db.delete(ContactContract.ContactEntry.TABLE_CONTACT, selection, selectionArgs);
    }
}

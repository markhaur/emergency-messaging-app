package com.example.molvi.emergencymessaging;

import android.provider.BaseColumns;

public final class ContactContract {

    private ContactContract(){}

    public static class ContactEntry implements BaseColumns {
        public static String TABLE_CONTACT = "contact";
        public static String COL_PHONE = "phone";
        public static String COL_NAME = "name";
    }
}

/**
 * Activity for adding numbers manually
 */
package com.example.molvi.emergencymessaging;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class ManualNumberAdd extends AppCompatActivity {

    private EditText txtName;       // EditText for name
    private EditText txtNumb;       // EditText for number
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual_number_add);
        setup();
    }

    // initializes the view elements
    private void setup(){
        txtName = (EditText) findViewById(R.id.txtContactName);
        txtNumb = (EditText) findViewById(R.id.txtNumber);
    }

    // action listener for back button
    public void onclick_btnBack(View view){
        this.finish();
    }

    // action listener for save button
    // save the number and sends the response to the parent activity
    public void onclick_btnSave(View view){
        String name = txtName.getText().toString();
        String numb = txtNumb.getText().toString();
        if(name.isEmpty() || numb.isEmpty()){
            Toast.makeText(this, "Required Information Missing", Toast.LENGTH_SHORT).show();
            return;
        }
        Intent intent = new Intent();
        intent.putExtra("name", name);
        intent.putExtra("numb", numb);
        setResult(RESULT_OK, intent);
        this.finish();
    }

}

/**
 * Template class for numbers
 * Class also implements Serialiazable for streams
 */
package com.example.molvi.emergencymessaging;

import java.io.Serializable;

public class Number implements Serializable{

    private String contactName;         // contact name
    private String contactNumb;         // contact number

    /**
     * Constructor
     * @param name
     * @param number
     */
    public Number(String name, String number){
        this.contactName = name;
        this.contactNumb = number;
    }

    /**
     * getter
     * @return contactName
     */
    public String getContactName(){
        return this.contactName;
    }

    /**
     * getter
     * @return contactNumb
     */
    public String getContactNumb(){
        return this.contactNumb;
    }

    /**
     * toString method
     * @return Current class representation
     */
    public String toString(){
        return contactName + "`" + contactNumb;
    }

    /**
     * equals method
     * @param other
     * @return  returns true if the numbers of two objects are same
     */
    public boolean equals(Number other){
        return other.contactNumb.equals(this.contactNumb);
    }

    /**
     * Clone the other object
     * @param newNumber
     */
    public void clone(Number newNumber){
        this.contactNumb = newNumber.getContactNumb();
        this.contactName = newNumber.getContactName();
    }
}

/**
 * This is the main Activity of my app
 * This activity will provide the user the ability to send emergency message on one click
 *
 * This class reads the emergency message stored in the app.
 * This class reads the contacts stored in app
 * This class will send message to contacts when clicked
 * This class also shares sender's location
 *
 */

package com.example.molvi.emergencymessaging;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {

    // request codes
    private final int MANUAL_REQ_CODE = 123;
    private final int PICK_CONTACT = 456;
    private final int NUMBER_VIEW = 789;

    private LocationManager locationManager;
    private LocationListener locationListener;
    private Location myLocation;
    private String locMsg = "My Location: ";

    private ArrayList<String> numbers;
    private String message;

    /**
     * This method will read the message and contacts stored in android app
     * This method will also set the user's current location
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        numbers = new ArrayList<>();
        message = "";
        setupNumbers();
        setupMessage();

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                myLocation = location;
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION}, 10);
            }
            return;
        }
        locationManager.requestLocationUpdates("gps", 5000, 0, locationListener);
    }

    /**
     * This method is responsible for reading the message
     */
    private void setupMessage(){
        try {
            FileInputStream fis = openFileInput("message.txt");
            Scanner reader = new Scanner(fis);
            while (reader.hasNext()){
                message += reader.nextLine();
            }
        } catch (Exception e) {
            //Toast.makeText(this, "" + e, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * This method is responsible for reading contacts
     */
    private void setupNumbers() {
        try {
            FileInputStream fis = openFileInput("phonebook.txt");
            Scanner reader = new Scanner(fis);
            String number = "";
            while (reader.hasNext()){
                number = reader.nextLine().split("`")[1];
                numbers.add(number);
            }
            reader.close();
            fis.close();
        } catch (Exception e) {
            //Toast.makeText(this, "" + e, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Method for loading menu layouts for this activity
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    /**
     * Method for implementing events for menus
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.message:
                Intent intent2 = new Intent(this, MessageActivity.class);
                startActivity(intent2);
                break;
            case R.id.conList:
                Intent intent = new Intent(this, ContactList.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Action listener for message send button
     * This method will send the message with user's current location
     * to the user's specified contacts
     * @param view
     */
    public void onclick_btnSendMessage(View view){
        if(message.isEmpty() || numbers.size() == 0){
            Toast.makeText(this, "Please add Message or Contacts", Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            SmsManager smsManager = SmsManager.getDefault();
            for(String number : numbers){
                Toast.makeText(this, "Sending Message To: " + number, Toast.LENGTH_SHORT).show();
                if(myLocation != null)
                    smsManager.sendTextMessage(number, null, message + "\n" + locMsg + "Lat: " + myLocation.getLatitude() + " Long: " + myLocation.getLongitude() , null, null);
                else
                    smsManager.sendTextMessage(number, null, message + "\n" + locMsg + "Lat: " + 29.378 + " Long: " + 71.760 , null, null);
            }
        }
        catch (Exception e){
            //Toast.makeText(this, "Error: Please give permissions for sending messages " + e, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        message = "";
        numbers = new ArrayList<>();
        setupMessage();
        setupNumbers();
    }
}

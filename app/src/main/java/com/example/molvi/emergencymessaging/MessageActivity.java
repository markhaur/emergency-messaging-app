/**
 * Activity for getting and saving message
 */
package com.example.molvi.emergencymessaging;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.Scanner;

public class MessageActivity extends AppCompatActivity {

    private final String FILE="message.txt";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        setup();
    }

    // setup the message field
    private void setup(){
        try {
            FileInputStream fis = openFileInput(FILE);
            Scanner reader = new Scanner(fis);
            String line = "";
            while (reader.hasNext()){
                line += reader.nextLine();
            }
            EditText txtMessage = (EditText) findViewById(R.id.txtMessage);
            txtMessage.setText(line);
        } catch (Exception e) {

        }
    }

    /**
     * action listener for back button
     * @param view
     */
    public void onclick_btnBack(View view){
        this.finish();
    }

    /**
     * action listener for save button
     * @param view
     */
    public void onclick_btnSave(View view){
        EditText txtMessage = (EditText) findViewById(R.id.txtMessage);
        String message = txtMessage.getText().toString();
        if(!message.isEmpty()){
            try {
                FileOutputStream fos = openFileOutput(FILE, Context.MODE_PRIVATE);
                OutputStreamWriter writer = new OutputStreamWriter(fos);
                writer.write(message);
                writer.close();
                Toast.makeText(this, "Message is saved", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Toast.makeText(this, "Can't save due to " + e, Toast.LENGTH_SHORT).show();
            }
        }
        else{
            Toast.makeText(this, "Please enter message", Toast.LENGTH_SHORT).show();
        }

    }
}

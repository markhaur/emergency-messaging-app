/**
 * Activity for viewing the number
 */
package com.example.molvi.emergencymessaging;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class NumberView extends AppCompatActivity {

    private EditText txtName;
    private EditText txtNumber;
    private Number number;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_number_view);
        setup();
    }

    /**
     * initializes the local variables and views
     */
    private void setup(){
        txtName = (EditText) findViewById(R.id.txtViewName);
        txtNumber = (EditText) findViewById(R.id.txtViewNumber);
        Intent intent = getIntent();
        number = (Number) intent.getSerializableExtra("data");
        txtName.setText(number.getContactName());
        txtNumber.setText(number.getContactNumb());
    }

    /**
     * action listener for back button
     * @param view
     */
    public void onclick_btnBack(View view){
        Intent intent = new Intent();
        intent.putExtra("res", 0);
        setResult(RESULT_OK, intent);
        this.finish();
    }

    /**
     * action listener for button update
     * @param view
     */
    public void onclick_btnUpdate(View view){
        String name = txtName.getText().toString();
        String number = txtNumber.getText().toString();
        Number newNumber = new Number(name, number);
        Intent intent = new Intent();
        intent.putExtra("data", this.number);
        intent.putExtra("newData", newNumber);
        intent.putExtra("res", 1);
        setResult(RESULT_OK, intent);
        this.finish();
    }

    /**
     * action listener for button remove
     * @param view
     */
    public void onlickc_btnRemove(View view){
        Intent intent = new Intent();
        intent.putExtra("data", number);
        intent.putExtra("res", 2);
        setResult(RESULT_OK, intent);
        this.finish();
    }
}

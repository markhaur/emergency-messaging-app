/**
 * This activity is responsible for maintaining contact lists
 * It will show the user's contact list
 */

package com.example.molvi.emergencymessaging;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class ContactList extends AppCompatActivity {

    private ListView phoneList;                     // to show contacts
    private ArrayList<Number> numbers;              // Store the Number instances
    private ArrayList<String> numberLayouts;        // ArrayList for adapter
    private ArrayAdapter<String> adapter;           // adapter which will be set to ListView
    private TextView txt = null;                    // TextView for showing info

    // response codes
    private final int back = 0;
    private final int update = 1;
    private final int remove = 2;

    // request codes
    private final int MANUAL_REQ_CODE = 123;
    private final int PICK_CONTACT = 456;
    private final int NUMBER_VIEW = 789;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);
        setup();
        setup_phonelist();
        setup_phonelist_listener();
        if(phoneList.getCount() == 0){
            txt = new TextView(this);
            LinearLayout mainLayout = (LinearLayout) findViewById(R.id.mainLayout);
            txt.setText("No contacts available");
            mainLayout.addView(txt);
        }
    }

    /**
     * initializes the local variables
     */
    private void setup(){
        this.phoneList = (ListView) findViewById(R.id.clist);
        numberLayouts = new ArrayList<>();
        numbers = new ArrayList<>();
        adapter = new ArrayAdapter<String>(ContactList.this, android.R.layout.simple_list_item_1, numberLayouts);
        phoneList.setAdapter(adapter);
    }

    /**
     * Initializes the contact phonelist
     * Reads contacts from the file
     */
    private void setup_phonelist(){
        try {
            FileInputStream fis = openFileInput("phonebook.txt");
            Scanner reader = new Scanner(fis);
            String line = "";
            while(reader.hasNext()){
                line = reader.nextLine();
                String name = line.split("`")[0];
                String phone = line.split("`")[1];
                numbers.add(new Number(name, phone));
                adapter.add(name + "\n" + phone);
            }
            adapter.notifyDataSetChanged();
            fis.close();
        } catch (Exception e) {
            //Toast.makeText(this, "Error " + e, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * adds action listener on ListView phonelist
     */
    private void setup_phonelist_listener(){
        Log.i("phone list listener", "Setup up phone list listener");
        phoneList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i("onitemselected", "on iitem selected");
                Number number = numbers.get(position);
                Log.i("Number Info", "Viewing Number " + number);
                Intent intent = new Intent(getApplicationContext(), NumberView.class);
                intent.putExtra("data", number);
                startActivityForResult(intent, NUMBER_VIEW);
            }
        });
    }

    /**
     * Method for loading menu file
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        return true;
    }

    /**
     * Method for implementing the events for menus
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.contactList:
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
                startActivityForResult(intent, PICK_CONTACT);
                break;

            case R.id.manual:
                Intent intent1 = new Intent(this, ManualNumberAdd.class);
                startActivityForResult(intent1, MANUAL_REQ_CODE);
                break;

            case R.id.message:
                Intent intent2 = new Intent(this, MessageActivity.class);
                startActivity(intent2);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * method for obtaining the result from other activities
     * @param requestCode which is sent while making request for result
     * @param resultCode determines if result of operation is OK
     * @param data in terms of intent
     */
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // code for handling result of ManualNumberAdd Activity
        if(requestCode == MANUAL_REQ_CODE && resultCode == RESULT_OK){
            if(txt != null)
                txt.setText("");
            String name = data.getStringExtra("name");
            String numb = data.getStringExtra("numb");
            adapter.add(name + "\n" + numb);
            adapter.notifyDataSetChanged();
            numbers.add(new Number(name, numb));
            update_phone_directory();
        }
        // code for handling result of getting contact from actual phone list
        else if(requestCode == PICK_CONTACT && resultCode == RESULT_OK && data != null){
            Uri contactData = data.getData();
            if(txt != null)
                txt.setText("");
            try{
                String id = contactData.getLastPathSegment();
                Log.i("ID Info", "Contact ID " + id);
                Cursor phoneCur = getContentResolver()
                        .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[] {id},
                                null);
                Log.i("PhoneCur Info", "phoneCur Size " + phoneCur.getCount());
                while (phoneCur.moveToNext()){
                    String phone = phoneCur.getString(phoneCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA));
                    String name = phoneCur.getString(phoneCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    Log.i("Phone Info", "Contact Phone " + phone);
                    Log.i("Name Info", "Contact Name " + name);
                    Number newNumber = new Number(name, phone);
                    if(!isExists(newNumber)){
                        adapter.add(name + "\n" + phone);
                        numbers.add(new Number(name, phone));
                    }
                }
                adapter.notifyDataSetChanged();
                update_phone_directory();
            }
            catch (Exception e){
                Log.e("Contact Exception", "Error " + e);
                //Toast.makeText(this, "Error, Please grant permission for contacts", Toast.LENGTH_SHORT).show();
            }
        }
        // code for handling response of NumberView activity
        else if(requestCode == NUMBER_VIEW && resultCode == RESULT_OK){
            int response = data.getIntExtra("res", -1);
            switch(response){

                case back:
                    break;
                case update:
                    Number number = (Number) data.getSerializableExtra("data");
                    Number newNumber = (Number) data.getSerializableExtra("newData");
                    for(Number num : numbers){
                        if(num.equals(number)){
                            num.clone(newNumber);
                            update_phone_directory();
                            updateAdapter();
                            break;
                        }
                    }
                    break;
                case remove:
                    Number number1 = (Number) data.getSerializableExtra("data");
                    for(Number num : numbers){
                        if(num.equals(number1)){
                            numbers.remove(num);
                            break;
                        }
                    }
                    update_phone_directory();
                    updateAdapter();
                    break;
            }
        }
    }

    /**
     * Check if number already exists in number list
     * @param newNumber
     * @return true if number already exists or false else
     */
    private boolean isExists(Number newNumber){
        for(Number number : numbers){
            if(number.equals(newNumber))
                return true;
        }
        return false;
    }

    // save the phone numbers
    private void update_phone_directory(){
        try {
            FileOutputStream fos = openFileOutput("phonebook.txt", MODE_PRIVATE);
            OutputStreamWriter writer = new OutputStreamWriter(fos);
            for(Number number : numbers){
                writer.write(number.toString() + "\n");
            }
            writer.close();
        } catch (FileNotFoundException e) {
            //e.printStackTrace();
        } catch (IOException e) {
            //e.printStackTrace();
        }

    }

    /**
     * method for updating the adapter
     */
    private void updateAdapter(){
        adapter.clear();
        for(Number number : numbers){
            String name = number.getContactName();
            String numb = number.getContactNumb();
            adapter.add(name + "\n" + numb);
        }
        adapter.notifyDataSetChanged();
    }

}
